import requests
import json
from datetime import datetime
import csv


class DriversLicense():
    
    def __init__(self, lastname, firstname, category, dateOfIssue, dateOfExpiration, isSuspended) -> None:
        self.lastname = lastname
        self.firstname = firstname
        self.category = category
        self.dateOfIssue = datetime.strptime(dateOfIssue, "%d/%m/%Y").date()
        self.dateOfExpiration = datetime.strptime(dateOfExpiration, "%d/%m/%Y").date()
        self.isSuspended = isSuspended

    def isExpired(self):
        return datetime.now().date() > self.dateOfExpiration
    

    def __repr__(self):
        return f"lastname : {self.lastname}, " +\
            f"firstname : {self.firstname}, " +\
            f"category : {self.category}, " +\
            f"dateOfIssue : {self.dateOfIssue}, " +\
            f"dateOfExpiration : {self.dateOfExpiration}, " +\
            f"isSuspended : {self.isSuspended}\n" 


class DataDownloader():

    def __init__(self, address) -> None:
        self.address = address

    def _load(self):
        data = requests.get(self.address)
        data = json.loads(data.content)
        licenses = []
        for d in data:
            license = DriversLicense(
                lastname=d["nume"],
                firstname=d["prenume"],
                category=d["categorie"],
                dateOfIssue=d["dataDeEmitere"],
                dateOfExpiration=d["dataDeExpirare"],
                isSuspended=d["suspendat"]
            )
            licenses.append(license)

        return licenses


    def load(self, numberOfDataPoints):
        licenses = []
        while len(licenses) < numberOfDataPoints:
            licenses += self._load()
        return licenses[:numberOfDataPoints]

def writeToCsv(filename, fields, rows):
    with open(f"{filename}.csv", "w", newline="") as file:
        writer = csv.writer(file, dialect="excel", delimiter=";")
        writer.writerow(fields)
        writer.writerows(rows)


def getSuspendedLicenses(licenses):
    suspendedLicenses = [l for l in licenses if l.isSuspended]
    print("Suspended Licenses:")
    print(suspendedLicenses)
    fields = ["lastname", "firstname", "category", "dateOfIssue", "dateOfExpiration", "isSuspended"]
    rows = []
    for l in suspendedLicenses:
        rows.append([l.lastname, l.firstname, l.category, l.dateOfIssue.strftime("%m/%d/%Y"), l.dateOfExpiration.strftime("%m/%d/%Y"), l.isSuspended])
    writeToCsv("suspendedLicenses", fields, rows)
        


def getValidLicenses(licenses):
    validLicenses = [l for l in licenses if(not l.isSuspended and not l.isExpired())]
    print("Valid Licenses")
    print(validLicenses)
    fields = ["lastname", "firstname", "category", "dateOfIssue", "dateOfExpiration", "isSuspended"]
    rows = []
    for l in validLicenses:
        rows.append([l.lastname, l.firstname, l.category, l.dateOfIssue.strftime("%m/%d/%Y"), l.dateOfExpiration.strftime("%m/%d/%Y"), l.isSuspended])
    writeToCsv("validLicenses", fields, rows)


def getCategories(licenses):
    categories = {}
    for l in licenses:
        if l.category in categories:
            categories[l.category] += 1;
        else:
            categories[l.category] = 1;
    print("License categories")
    print(categories)
    fields = ["category", "count"]
    rows = []
    for c, count in categories.items():
        rows.append([c, count])
    writeToCsv("categories", fields, rows)


if __name__ == "__main__":
    dataDownloader = DataDownloader("http://localhost:30000/drivers-licenses/list")
    licenses = dataDownloader.load(150)

    operationId = input("Enter your desired operation ID:\n1 - get suspended licenses\n2 - get valid licenses\n3 - get license categories\n")
    if operationId == "1":
        getSuspendedLicenses(licenses)
    elif operationId == "2":
        getValidLicenses(licenses)
    elif operationId == "3":
        getCategories(licenses)
    else:
        print("Incorrect operation ID")


